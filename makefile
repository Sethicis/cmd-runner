
.PHONEY: deploy-to-test
deploy-to-test: clean run-tests build-dist
	twine upload --repository-url https://test.pypi.org/legacy/ dist/*

.PHONEY: deploy-to-prod
deploy-to-prod: clean run-tests build-dist
	twine upload dist/*

.PHONEY: build-dist
build-dist:
	python3 setup.py sdist bdist_wheel

.PHONEY: run-tests
run-tests:
	@echo 'Running tests...'

.PHONEY: update-dependencies
update-dependencies:
	@echo 'Updating dependencies...'
	python3 -m pip install --user --upgrade setuptools wheel twine

.PHONEY: clean
clean:
	rm -rf ./build ./dist
