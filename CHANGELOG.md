# Version 0.0.2

## Added new features to help with registering handlers related to specific system signals.
These new features are essentially just a wrapper on top of the internal Python 3 signal module.
I've built in some OOP principles to assist the user and IDE in determining what arguments should be provided to a signal handler, and determining whether or not an handler has already been registered for a specific signal.

### New classes/objects of note.
* exit_handler_registry
    * Singleton object that offers a set of methods for adding signal handlers and checking if a signal handler already exists.
* AbstractExitHandler
    * New abstract "static" class that functions as a contract with the exit_handler_registry. The abstract method "exit_handler" is a static method, so don't forget to add the appropriate decorator when extending this class.
* Signals
    * A new Enum class that contains three of the most common system signals.  This class was created to help with the tooling when working with signal handlers.
