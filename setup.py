import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="cmd_runner",
    version="0.0.2",
    author="sethicis",
    author_email="noahjemkai@gmail.com",
    description="Small set of helper functions for python scripts",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/Sethicis/cmd-runner.git",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
