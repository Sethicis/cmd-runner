# <span style="color:lightblue">Command Runner (cmd\_runner)</span>

## <span style="color:lightblue">Description</span>
The purpose of this PIP module was to unify a set of common helper functions I found myself writing for my own personal projects and scripts.  Command Runner's main feature is the `run_cmd` which acts as a wrapper around a shell based subprocess execution with hooks for a failure callback and success callback.  In the case of a failure the function returns `False`, but when a subprocess returns an exit code of `0` the function will actually return the `subprocess.CompletedProcess` object.
There are a number of other small helper functions that are listed in the `Functions` section of this README, but I won't get into them here.
This PIP package is not my primary focus, so for version 0.0.2 I'm just kind of throwing this thing together, but when I get a chance I'll be cleaning things up a bit, adding tests where possible, and adding additional features.
I will be honoring semantic versioning, so breaking changes will not be included in minor version releases.  Mid version changes may contain new behavior or, but compatibility cannot be guarenteed.  Major version changes should be considered breaking and are not tested for backwards compatibility.
I will do my best to call out any new features or bug fixes in a release.

## <span style="color:lightblue">Functions</span>
### <span style="color:lightgreen">run_cmd</spanspan>
```python
def run_cmd(command: str, failure_cb: callable = None, success_cb: callable = None) -> Union[bool, subprocess.CompletedProcess]:
```
#### Description
A helper designed to execute shell commands and provide hooks for executing follow-up operations dependent on whether or not the subprocess was successful (exit status 0) or failed (non-zero exit status).

#### Arguments
* <strong>command</strong>: The shell command string.
* <strong>failure_cb</strong>: Optional callable that is used if the command returns a non-zero exit code.  Callable will receive the exit code of the subprocess as the first argument and the captured output as the second.
* <strong>success_cb</strong>: Optional callable that is used if the command returns a zero exit code.  Callable will receive the captured output of the executed subprocess.  Note, this information can also be retrieved from the `CompletedProcess` object returned by the function as well.
#### Return
* This function returns the `subprocess.CompleteProcess` object in all cases where the executed subprocess returns a zero exit status.  If the executed subprocess returns a non-zero exit status this function returns `False`.

### <span style="color:lightgreen">now</span>
```python
def now() -> str:
```
#### Description
Helper function that returns the current date time in UTC using the following format: `dd-mm-YYYY HH:MM:SS`.
This function is mostly used by the `log` and `log_error` functions.

#### Return
String representation of the current date and time in UTC timezone. Format `dd-mm-YYYY HH:MM:SS`.

### <span style="color:lightgreen">log</span>
```python
def log(msg: str) -> None:
```
#### Description
Helper function that handles logging a message string to `stdout` with the current date and time in UTC prepended to the string.
Log entry format will be "`dd-mm-YYYY HH:MM:SS > [LOG-MSG]`".

### <span style="color:lightgreen">log_error</span>
```python
def log_error(err_msg: str) -> None:
```
#### Description
Helper function that handles logging a message string to `stderr` with the current date and time in UTC prepended to the string.
Log entry format will be "`dd-mm-YYYY HH:MM:SS > [LOG-MSG]`".

### <span style="color:lightgreen">terminate</span>
```python
def terminate(code: int = 1, msg: str = None) -> None:
```
#### Description
Helper function that can log a message to `stderr` and and then exit your script with a provided error code.

#### Arguments
* <strong>code</strong>: Integer value for the exit status to use. If one is not provided an exit status of `1` will be used by default.
* <strong>msg</strong>: Optional error message to log to `stderr` before terminating the script/app.

### <span style="color:lightgreen">Signals</span>
```python
class Signals(Enum):
```
#### Description
Enum class containing three of the most common system signals: SIGINT, SIGTERM and SIGQUIT.  This enum is designed to be used in conjunction with the `exit_handler_registry`.

### <span style="color:lightgreen">AbstractAtExit</span>
```python
class AbstractAtExit(metaclass=ABCMeta):
```
#### Description
This class serves as a contract to be used with the ExitHandlerRegistry.
Applications that wish to register some function to run on a system signal
should create a new subclass of this "static" class and implement "exit_handler".
"exit_handler" is a static method that expects two arguments, signum (int) and frame (Optional[any]).
For more details read the [Python documentation on "signal.signal".](https://docs.python.org/3/library/signal.html#…)

### <span style="color:lightgreen">register_exit_handler_for_signal</span>
```python
def register_exit_handler_for_signal(signum: Signals, handler: AbstractAtExit, force: bool = False, ignore_exit: bool = False) -> None:
```
#### Description
Registers a signal handler for the application.
If the provided signal already has a handler registered, then a `InUseSignalException` will be raised.

#### Arguments
* <strong>signum</strong>: A Signals enum object for the type of signal to register the handler for.
* <strong>handler</strong>: An instance of `AbstractAtExit`, which enforces the signal handler contract.
* <strong>force</strong>: If true, overwrites an existing handler if one is already set for the given signal.
* <strong>ignore_exit</strong>: If true, the wrapper function that makes the application exit after the handler is called will <i>NOT</i> be added.

## <span style="color:lightblue">Contributing</spanspan>
This is a public project, so should you wish to contribute or just fork this project, be my guest.  I just ask that if you use any of my work please give credit where due.
When making pull requests back to this project please run all unit tests prior to opening your PR (I know there are none right now), and state clearly in your pull request what the change you are making corrects or adds.  Pull requests should be opened to the "dev" branch of this project, not to "master" directly.
