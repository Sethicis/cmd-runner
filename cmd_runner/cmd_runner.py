from datetime import datetime
import subprocess
import sys
from typing import Union


def now() -> str:
    """Returns the UTC date time representation of now."""
    return datetime.utcnow().strftime('%d-%m-%Y %H:%M:%S')


# Define command runner
def run_cmd(command: str, failure_cb: callable = None, success_cb: callable = None) -> Union[bool, subprocess.CompletedProcess]:
    try:
        sub_process = subprocess.run(command, shell=True, check=True, capture_output=True)
    except subprocess.CalledProcessError as err:
        if failure_cb:
            failure_cb(err.returncode, err.stderr)
        return False

    if success_cb:
        success_cb(sub_process.stdout)
    return sub_process


def log_error(msg: str):
    """Log message to standard error."""
    print('{0} > {1}'.format(now(), msg), file=sys.stderr)


def log(msg: str):
    """Log message to standard out."""
    print('{0} > {1}'.format(now(), msg))

def terminate(code: int = 1, msg: str = None):
    if msg:
        log_error(msg)
    exit(code)
