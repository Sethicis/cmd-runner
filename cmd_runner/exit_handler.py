from abc import ABCMeta, abstractmethod
from enum import Enum
import signal
from typing import Callable


class Signals(Enum):
    SIGINT = signal.SIGINT
    SIGTERM = signal.SIGTERM
    SIGQUIT = signal.SIGQUIT


class AbstractAtExit(metaclass=ABCMeta):
    """
    This class serves as a contract to be used with the ExitHandlerRegistry.
    Applications that wish to register some function to run on a system signal
    should create a new subclass of this "static" class and implement "exit_handler".
    "exit_handler" is a static method that expects two arguments, signum (int) and frame (Optional[any]).
    For more details read the Python documentation on "signal.signal".
    """
    @staticmethod
    @abstractmethod
    def exit_handler(signum: int, frame = None):
        pass



class InUseSignalException(Exception):
    
    def __init__(self, sig: Signals) -> None:
        self.signal = sig
    
    def __str__(self) -> str:
        return """
        The signal you are trying to register '{0}' is already assigned a handler.
        If you you would like to override call this method again with 'force' set to 'True'.
        """.format(self.signal.name)


class ExitHandlerRegistry():
    """
    Signal Registry singleton for adding handlers to a specific system signals.
    For more information see the Python 3 documentation on "signal.signal".
    """
    def __init__(self):
        self.handlers = {}

    def register_exit_handler_for_signal(self, signum: Signals, handler: AbstractAtExit, force: bool = False, ignore_exit: bool = False) -> None:
        """
        Registers a signal handler for the application.
        If the provided signal already has a handler registered, then a InUseSignalException will be raised.
        """
        if not force and self.has_handler(signum):
            raise InUseSignalException(signum)

        handler_callable = self.__exit_handler_factory(handler, ignore_exit=ignore_exit)
        signal.signal(signum.value, handler_callable)
        self.handlers[signum.value] = handler_callable
    
    def has_handler(self, signum: Signals) -> bool:
        """Checks if the given signal already has a registered handler."""
        return self.handlers.get(signum.value) is not None
    
    def clear_handlers(self) -> None:
        """Clears all registered handlers and restores the Python default handler."""
        default = signal.SIG_DFL
        [signal.signal(signum, default) for signum in self.handlers]
        self.handlers.clear()
        signal.signal(Signals.SIGINT.value, signal.default_int_handler)

    def __exit_handler_factory(self, handler: AbstractAtExit, ignore_exit: bool = False) -> Callable:
        """
        This factory adds a wrapper function to the provided exit handler to
        make sure the program terminates after running the handler.
        If "ignore_exit" is set to "True", then the wrapper will not be added.
        """
        def wrapper(signum: int, frame = None):
            handler.exit_handler(signum, frame)
            exit()
        
        return wrapper if not ignore_exit else handler.exit_handler


# Create singleton instance.
exit_handler_registry = ExitHandlerRegistry()

def register_exit_handler_for_signal(signum: Signals, handler: AbstractAtExit, force: bool = False, ignore_exit: bool = False) -> None:
    """
    Registers a signal handler for the application.
    If the provided signal already has a handler registered, then a InUseSignalException will be raised.
    """
    exit_handler_registry.register_exit_handler_for_signal(signum=signum, handler=handler, force=force, ignore_exit=ignore_exit)